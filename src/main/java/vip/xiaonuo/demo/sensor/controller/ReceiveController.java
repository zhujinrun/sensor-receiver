package vip.xiaonuo.demo.sensor.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import vip.xiaonuo.demo.core.pojo.ResponseData;
import vip.xiaonuo.demo.core.pojo.SuccessResponseData;
import vip.xiaonuo.demo.core.socket.WebSocketServer;
import vip.xiaonuo.demo.sensor.entity.Gas;
import vip.xiaonuo.demo.sensor.entity.Smoke;
import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;
import vip.xiaonuo.demo.sensor.model.GasSensorModel;
import vip.xiaonuo.demo.sensor.model.SmokeSensorModel;
import vip.xiaonuo.demo.sensor.model.TemperatureAndHumiditySensorModel;
import vip.xiaonuo.demo.sensor.service.GasService;
import vip.xiaonuo.demo.sensor.service.SmokeService;
import vip.xiaonuo.demo.sensor.service.TemperatureAndHumidityService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * 传感器接收器控制器
 *
 * @author zhujinrun
 * @since 2021/11/07 13:40
 */
@RestController
@RequestMapping("/receive")
@Slf4j
public class ReceiveController {

    @Resource
    private TemperatureAndHumidityService temperatureAndHumidityService;
    @Resource
    private GasService gasService;
    @Resource
    private SmokeService smokeService;

    private Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    /**
     * 接收器心跳检测
     *
     * @return ResponseData
     * @author zhujinrun
     * @since 2021/11/07 13:41
     */
    @GetMapping
    public ResponseData get() {
        log.info("sensor receive...");
        return new SuccessResponseData();
    }

    /**
     * 温湿度传感器接收
     *
     * @return ResponseData
     * @author zhujinrun
     * @since 2021/11/07 13:42
     */
    @PostMapping("/temperature_and_humidity")
    public ResponseData temperatureAndHumidityPost(@RequestBody TemperatureAndHumiditySensorModel temperatureAndHumiditySensorModel) {
        log.info("temperatureAndHumiditySensorModel = {}", gson.toJson(temperatureAndHumiditySensorModel));
        TemperatureAndHumidity temperatureAndHumidity = temperatureAndHumiditySensorModel.convertToMongoModel();
        return new SuccessResponseData(temperatureAndHumidityService.save(temperatureAndHumidity));
    }

    @PostMapping("/temperature_and_humidity2")
    public ResponseData temperatureAndHumidityPost2(@RequestBody Object temperatureAndHumiditySensorModel) {
        log.info("temperatureAndHumiditySensorModel = {}", gson.toJson(temperatureAndHumiditySensorModel));
        return new SuccessResponseData(temperatureAndHumiditySensorModel);
    }

    /**
     * 烟感报警器接收
     *
     * @author zhujinrun
     * @since 2021/11/22 10:28
     */
    @PostMapping("/smoke_and_fire")
    public ResponseData smokePost(@RequestBody SmokeSensorModel smokeSensorModel) {
        log.info("smokeSensorModel = {}", gson.toJson(smokeSensorModel));
        Smoke smoke = smokeSensorModel.convertToMongoModel();
        return new SuccessResponseData(smokeService.save(smoke));
    }

    @PostMapping("/smoke_and_fire1")
    public ResponseData smokePost2(@RequestBody Object smokeSensorModel) {
        log.info("smokeSensorModel = {}", gson.toJson(smokeSensorModel));
        return new SuccessResponseData(smokeSensorModel);
    }

    /**
     * 燃气报警器
     *
     * @author zhujinrun
     * @since 2021/11/22 10:31
     */
    @PostMapping("/gas")
    public ResponseData gasPost(@RequestBody GasSensorModel gasSensorModel) {
        log.info("gasSensorModel = {}", gson.toJson(gasSensorModel));
        Gas gas = gasSensorModel.convertToMongoModel();
        return new SuccessResponseData(gasService.save(gas));
    }

    @PostMapping("/gas2")
    public ResponseData gasPost2(@RequestBody Object gasSensorModel) {
        log.info("gasSensorModel = {}", gson.toJson(gasSensorModel));
        return new SuccessResponseData(gasSensorModel);
    }

    /**
     * WebSocket消息推送
     *
     * @return ResponseData
     * @author zhujinrun
     * @since 2021/11/07 13:41
     */
    @PostMapping("/socket-push/{cid}")
    public ResponseData pushMessage(@PathVariable String cid, String message) {
        log.info("socket push...");
        Map<String, Object> result = new HashMap<>();
        try {
            HashSet<String> sids = new HashSet<>();
            sids.add(cid);
            WebSocketServer.sendMessage("服务端推送消息：" + message, sids);
            result.put("code", cid);
            result.put("msg", message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new SuccessResponseData(result);
    }
}
