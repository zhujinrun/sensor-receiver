package vip.xiaonuo.demo.sensor.entity;

import lombok.Data;
import vip.xiaonuo.demo.sensor.model.SensorModel;

import java.util.Date;

@Data
public class Smoke extends SensorModel {

    private Date timestamp;

    private String concentration;
}
