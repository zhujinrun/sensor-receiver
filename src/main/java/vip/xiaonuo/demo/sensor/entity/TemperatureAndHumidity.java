package vip.xiaonuo.demo.sensor.entity;

import lombok.Data;
import vip.xiaonuo.demo.sensor.model.SensorModel;

import java.util.Date;

@Data
public class TemperatureAndHumidity extends SensorModel {

    private Date timestamp;

    private String temperature;

    private String humidity;
}
