package vip.xiaonuo.demo.sensor.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import vip.xiaonuo.demo.sensor.entity.Smoke;
import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;

public interface SmokeRepository extends MongoRepository<Smoke, String> {
}
