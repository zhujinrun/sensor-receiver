package vip.xiaonuo.demo.sensor.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;

public interface TemperatureAndHumidityRepository extends MongoRepository<TemperatureAndHumidity, String> {
}
