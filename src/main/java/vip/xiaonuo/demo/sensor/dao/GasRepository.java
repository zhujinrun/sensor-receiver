package vip.xiaonuo.demo.sensor.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import vip.xiaonuo.demo.sensor.entity.Gas;
import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;

public interface GasRepository extends MongoRepository<Gas, String> {
}
