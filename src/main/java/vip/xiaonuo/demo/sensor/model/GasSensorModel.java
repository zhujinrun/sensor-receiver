package vip.xiaonuo.demo.sensor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import vip.xiaonuo.demo.sensor.entity.Gas;

/**
 * 燃气传感器数据对象
 *
 * @author zhujinrun
 * @since 2021/11/22 10:41
 */
@Slf4j
public class GasSensorModel extends SensorModel {

    @JsonIgnore
    public Gas convertToMongoModel() {
        Gas gas = new Gas();
        BeanUtils.copyProperties(this, gas);
        gas.setTimestamp(this.getPropTimestamp());
        gas.setTemperature(this.getPropDataItemValue("sensor2", "sensor_value"));
        gas.setMethane(this.getPropDataItemValue("sensor1", "sensor_value"));
        return gas;
    }
}
