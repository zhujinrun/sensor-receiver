package vip.xiaonuo.demo.sensor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;

/**
 * 温湿度传感器数据对象
 *
 * @author zhujinrun
 * @since 2021/11/07 13:39
 */
@Slf4j
public class TemperatureAndHumiditySensorModel extends SensorModel {

    @JsonIgnore
    public TemperatureAndHumidity convertToMongoModel() {
        TemperatureAndHumidity temperatureAndHumidity = new TemperatureAndHumidity();
        BeanUtils.copyProperties(this, temperatureAndHumidity);
        temperatureAndHumidity.setTimestamp(this.getPropTimestamp());
        temperatureAndHumidity.setTemperature(this.getPropDataValue("AIR_TEMP").toString());
        temperatureAndHumidity.setHumidity(this.getPropDataValue("AIR_HUMI").toString());
        return temperatureAndHumidity;
    }
}