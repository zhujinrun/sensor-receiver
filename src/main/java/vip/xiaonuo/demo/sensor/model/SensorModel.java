package vip.xiaonuo.demo.sensor.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * 传感器数据对象
 *
 * @author zhujinrun
 * @since 2021/11/22 10:31
 */
@Slf4j
@Data
public class SensorModel implements Serializable {

    @JsonProperty("prop_data")
    private SensorPropData[] propData;

    @JsonProperty("device_key")
    private String deviceKey;
    @JsonProperty("product_key")
    private String productKey;
    @JsonProperty("product_name")
    private String productName;

    @JsonIgnore
    public SensorPropData getPropData(String key) {
        Optional<SensorPropData> propData = Arrays.stream(this.propData).filter(p -> p.getDevicePropKey().equals(key)).findFirst();
        return propData.orElseGet(null);
    }

    @JsonIgnore
    public Object getPropDataValue(String key) {
        SensorPropData propData = getPropData(key);
        return propData != null ? propData.getDevicePropValue() : null;
    }

    @JsonIgnore
    public Date getPropTimestamp() {
        Optional<Date> devicePropTs = Arrays.stream(this.propData).map(SensorPropData::getDevicePropTs).findFirst();
        log.info("Timestamp = {}", devicePropTs.get());
        return devicePropTs.orElseGet(Date::new);
    }

//    public Object mapToObject(Map<String, Object> map) {
//        if (map == null)
//            return null;
//        return JSON.toJSON(map);
//    }

//    public Map<String, Object> objectToMap(Object obj) {
//        if (obj == null)
//            return null;
//        return JSONObject.parseObject(JSON.toJSONString(obj));
//    }

    @JsonIgnore
    public String getPropDataItemValue(String key, String paramName) {
        Object model = getPropDataValue(key);
        if (model == null)
            return "";
        Map<String, Object> entry = JSONObject.parseObject(JSON.toJSONString(model));
        return entry.getOrDefault(paramName, "").toString();
    }
}
