package vip.xiaonuo.demo.sensor.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SensorPropData implements Serializable {
    @JsonProperty("device.prop.value")
    private Object devicePropValue;
    @JsonProperty("device.prop.ts")
    private Date devicePropTs;
    @JsonProperty("device.prop.name")
    private String devicePropName;
    @JsonProperty("device.prop.key")
    private String devicePropKey;
}
