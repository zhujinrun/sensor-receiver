package vip.xiaonuo.demo.sensor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import vip.xiaonuo.demo.sensor.entity.Smoke;

/**
 * 烟雾传感器数据对象
 *
 * @author zhujinrun
 * @since 2021/11/22 10:42
 */
@Slf4j
public class SmokeSensorModel extends SensorModel {

    @JsonIgnore
    public Smoke convertToMongoModel() {
        Smoke smoke = new Smoke();
        BeanUtils.copyProperties(this, smoke);
        smoke.setTimestamp(this.getPropTimestamp());
        smoke.setConcentration(this.getPropDataValue("smokeConcentration").toString());
        return smoke;
    }
}
