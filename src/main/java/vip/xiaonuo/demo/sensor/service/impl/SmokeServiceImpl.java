package vip.xiaonuo.demo.sensor.service.impl;

import org.springframework.stereotype.Service;
import vip.xiaonuo.demo.sensor.dao.SmokeRepository;
import vip.xiaonuo.demo.sensor.entity.Smoke;
import vip.xiaonuo.demo.sensor.service.SmokeService;

import javax.annotation.Resource;

@Service
public class SmokeServiceImpl implements SmokeService {

    @Resource
    private SmokeRepository SmokeRepository;

    @Override
    public Smoke save(Smoke Smoke) {
        return SmokeRepository.save(Smoke);
    }
}
