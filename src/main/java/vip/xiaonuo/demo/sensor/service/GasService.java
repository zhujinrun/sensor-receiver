package vip.xiaonuo.demo.sensor.service;

import vip.xiaonuo.demo.sensor.entity.Gas;

public interface GasService {
    Gas save(Gas gas);
}
