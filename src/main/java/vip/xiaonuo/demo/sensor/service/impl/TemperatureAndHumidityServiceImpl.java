package vip.xiaonuo.demo.sensor.service.impl;

import org.springframework.stereotype.Service;
import vip.xiaonuo.demo.sensor.dao.TemperatureAndHumidityRepository;
import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;
import vip.xiaonuo.demo.sensor.service.TemperatureAndHumidityService;

import javax.annotation.Resource;

@Service
public class TemperatureAndHumidityServiceImpl implements TemperatureAndHumidityService {

    @Resource
    private TemperatureAndHumidityRepository temperatureAndHumidityRepository;

    @Override
    public TemperatureAndHumidity save(TemperatureAndHumidity temperatureAndHumidity) {
        return temperatureAndHumidityRepository.save(temperatureAndHumidity);
    }
}
