package vip.xiaonuo.demo.sensor.service;

import vip.xiaonuo.demo.sensor.entity.Smoke;

public interface SmokeService {
    Smoke save(Smoke smoke);
}
