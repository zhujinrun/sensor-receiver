package vip.xiaonuo.demo.sensor.service.impl;

import org.springframework.stereotype.Service;
import vip.xiaonuo.demo.sensor.dao.GasRepository;
import vip.xiaonuo.demo.sensor.entity.Gas;
import vip.xiaonuo.demo.sensor.service.GasService;

import javax.annotation.Resource;

@Service
public class GasServiceImpl implements GasService {

    @Resource
    private GasRepository GasRepository;

    @Override
    public Gas save(Gas Gas) {
        return GasRepository.save(Gas);
    }
}
