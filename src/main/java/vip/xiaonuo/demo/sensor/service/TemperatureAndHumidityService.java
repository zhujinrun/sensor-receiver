package vip.xiaonuo.demo.sensor.service;

import vip.xiaonuo.demo.sensor.entity.TemperatureAndHumidity;

public interface TemperatureAndHumidityService {
    TemperatureAndHumidity save(TemperatureAndHumidity temperatureAndHumidity);
}
